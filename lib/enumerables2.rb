require 'set'
# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0, &:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |word| word.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  unique_letter(string).sort
end

def unique_letter(string)
  string.chars.reduce([]) do |u_chars, elem|
    next u_chars if !u_chars.index(elem).nil? || elem == " "
    u_chars << elem if string.count(elem) > 1
    u_chars
  end
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  sorted_words = string.split(" ").sort_by(&:length)
  sorted_words[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ("a".."z").to_a
  string.chars { |char| alphabet.delete(char)}
  alphabet
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.select { |el| not_repeat_year? el}
end

def not_repeat_year?(year)
  year.to_s.chars.each { |el| return false if year.to_s.count(el) > 1}
  true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  non_consecutive_week_songs!(songs.dup).to_set
end

def consecutive_week_songs(songs)
  non_one_week_wonders = Set.new
  (0...(songs.length - 1)).each do |index|
    current_w_song = songs[index]
    next_w_song = songs[index + 1]
    if current_w_song == next_w_song
      non_one_week_wonders.add(current_w_song)
    end
  end
  non_one_week_wonders.to_a
end

def non_consecutive_week_songs!(songs)
  consecutive_week_songs(songs).each { |song| songs.delete(song) }
  songs
end

def no_repeats?(song_name, songs)
  songs.count(song_name) == 1
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.
def for_cs_sake(string)
  process_words(string)
end

def process_words(string)
  string.split(" ").reduce(String.new) do |min_dist_word, word|
    word = clean_word(word)
    if word.count("c") > 0
      if min_dist_word.empty? || (c_distance(word) < c_distance(min_dist_word))
      word
      else
        min_dist_word
      end
    else
      min_dist_word
    end
  end
end

def c_distance(word)
  (word.length - word.rindex("c") - 1)
end

def clean_word(word)
  word.gsub(/\W/, "")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  start_idx = nil
  res = []
  (0...(arr.length - 1)).each do |index|
    # debugger
    if arr[index] == arr[index + 1]
      if start_idx.nil?
        start_idx = index
      end
    elsif !start_idx.nil?
      res << [start_idx, index]
      start_idx = nil
    end
  end

  if !start_idx.nil?
    res << [start_idx, arr.length - 1]
  end
  res

end
